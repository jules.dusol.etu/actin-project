import sys
import argparse
import os

parser=argparse.ArgumentParser(prog="SeparateCluster.py")
parser.add_argument("-f","--xmgracefile", help="xmgrace file containing the residues contribution to rmsd;" )
parser.add_argument("-o","--outputfile",help="file for output data;")
args = parser.parse_args(sys.argv[1:])


#Create a file at a given location
#
#Parameters
#-----------
#location : string
#    The location of the file
#
#content : string
#    The content to write in the file
def createFile(location, content):
    with open(location, 'w') as file:
        file.write(content)
        file.close()


#Get the data from the xmgrace file
#
#Parameters
#-----------
#inputFile : string
#    The name of the xmgracefile
#
#outputFile : string
#	 The file name for output
#   
#Return
#----------
def ExtractData(inputFile,outputFile):
	data = {}
	total = 0
	with open(inputFile) as xmgFile :
		for line in xmgFile:
			if line[0] != "#" and line[0] != "@":
				line = line.split()
				data[line[0]] = float(line[1])
				total += float(line[1])
	mean = total / len(data)
	outputData = sorted(data.items(), key=lambda x:x[1], reverse=True)
	outputText = 'Mean is  ' +str(mean) + '\n\nResidue\tContribution\n'
	for result in outputData:
		outputText += result[0] + '\t' + str(result[1]) + '\n'
	createFile(outputFile,outputText)
		
				
if __name__=="__main__":
	xmgFile = args.xmgracefile
	outputFile = args.outputfile
	ExtractData(xmgFile,outputFile)
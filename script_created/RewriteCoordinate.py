import sys
import argparse
import os

parser=argparse.ArgumentParser(prog="SeparateCluster.py")
parser.add_argument("-f","--pdbfile", help="pdb file containing the coordinates;" )
parser.add_argument("-o","--outputfile",help="grofile for output;default = ligand.gro",default = "ligand.gro" )
args = parser.parse_args(sys.argv[1:])


#Create a file at a given location
#
#Parameters
#-----------
#location : string
#    The location of the file
#
#content : string
#    The content to write in the file
def createFile(location, content):
    with open(location, 'w') as file:
        file.write(content)
        file.close()


#Get the data from the pdb file
#
#Parameters
#-----------
#inputFile : string
#    The name of the pdb file
#
#outputFile : string
#	 The file name for output
def ExtractData(inputFile,outputFile):
	outputText = '10ANP_ATP.pdb.mol2_GMX.gro created by acpype (v: 2022.7.21) on Thu May 18 11:10:09 2023\n' 
	total = 0
	toAdd = ''
	with open(inputFile) as pdbFile :
		for line in pdbFile:
			if line != "END\n":
				line = line.split()
				toAdd += '    ' + line[3] 
				if len(line[2]) == 2:
					toAdd +='   '+line[2]
				else:
					toAdd +='  '+line[2]
				if len(line[1]) == 1:
					toAdd +='    '+line[1]
				else:
					toAdd +='   '+line[1]
				x = str(round(float(line[5])/10,3))
				y = str(round(float(line[6])/10,3))
				z = str(round(float(line[7])/10,3))
				toAdd += '   '+x+'   '+y+'   '+z+'\n'
				total += 1
	outputText += str(total) + '\n' + toAdd
	createFile(outputFile,outputText)
		
				
if __name__=="__main__":
	pdbFile = args.pdbfile
	outputFile = args.outputfile
	ExtractData(pdbFile,outputFile)
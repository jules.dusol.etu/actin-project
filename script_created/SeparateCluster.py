import sys
import argparse
import os

parser=argparse.ArgumentParser(prog="SeparateCluster.py")
parser.add_argument("-pdb","--pdbfile", help="pdb file containing all the cluster's model;" )
parser.add_argument("-dir","--directory",help="directory in which register the different model;")
args = parser.parse_args(sys.argv[1:])


#Create a file at a given location
#
#Parameters
#-----------
#location : string
#    The location of the file
#
#content : string
#    The content to write in the file
def createFile(location, content):
    with open(location, 'w') as file:
        file.write(content)
        file.close()


#Get the data from the pdb file
#
#Parameters
#-----------
#inputFile : string
#    The name of the csv file
#
#directory : string
#	 The directory where to store the pdb model
#   
#Return
#----------
def ExtractData(inputFile,directory):
	with open(inputFile) as pdbFile :
		textToCopy = ''
		modelNumber = 0
		for line in pdbFile:
			textToCopy += line
			if line == 'ENDMDL\n' :
				modelNumber += 1
				createFile(directory+os.path.sep+'model'+str(modelNumber)+'.pdb',textToCopy)
				textToCopy = ''
				
if __name__=="__main__":
	pdbFile = args.pdbfile
	directory = args.directory
	ExtractData(pdbFile,directory)
import sys
import argparse
import os
import matplotlib.pyplot as plt
import itertools


parser=argparse.ArgumentParser(prog="SeparateCluster.py")
parser.add_argument("-n","--filenumber", help="number of file to grab from water_n.pdb;" )
parser.add_argument("-o","--outputfile",help="file for output data;")
args = parser.parse_args(sys.argv[1:])


#Create a file at a given location
#
#Parameters
#-----------
#location : string
#    The location of the file
#
#content : string
#    The content to write in the file
def createFile(location, content):
    with open(location, 'w') as file:
        file.write(content)
        file.close()
			
if __name__=="__main__":
	n = int(args.filenumber)
	i = 0
	index_number = {}
	while i < n + 1 :
		with open("water_"+str(i)+".pdb") as pdbFile :
			for line in pdbFile:
				if line[0:4] == "ATOM":
					line = line.split()
					if line[2] == "OW":
						if line[4][0] == "X":
							line[4] = line[4][1:]
						if line[4] == "" :
							line[4] = line[5]
						if line[4] in index_number:
							index_number[line[4]].append(int(i))
						else:
							index_number[line[4]] = [int(i)]
		i += 1
	print(index_number)
	x= []
	y= []
	for k, v in index_number.items():
		y.extend(list(itertools.repeat(k, len(v))))
		x.extend(v)
	plt.xlim(0,5)
	plt.scatter(x,y,marker = 's', s = 50, c = '#000000')
	print(y,x)
	plt.savefig(args.outputfile)

	

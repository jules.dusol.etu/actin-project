#python test.py -pdb frame0.pdb -xtc clean.xtc -f True -l True -c 10 -t mg -o test
import sys
import argparse
import os
import matplotlib.pyplot as plt
import itertools
import numpy as np
import pandas as pd
import MDAnalysis as mda
from MDAnalysis.analysis import align, rms
import time

#Prendre en compte water ?


parser=argparse.ArgumentParser(prog="dihedral.py")
parser.add_argument("-pdb","--pdbfile", help="topology file that contains the data;" )
parser.add_argument("-xtc","--xtcfile", help="trajectory file that contains the data;" )
parser.add_argument("-o","--outputfile",help="file for output names;")
args = parser.parse_args(sys.argv[1:])


#Create a file at a given location
#
#Parameters
#-----------
#location : string
#    The location of the file
#
#content : string
#    The content to write in the file
def createFile(location, content):
    with open(location, 'w') as file:
        file.write(content)
        file.close()
	
#Check how much and how many residue are in cutoff distance
#
#Parameters
#-----------
#u : Universe
#    Universe object containing all the residue to check
#
#target : atom selection
#	The target from which to check the cutoff distance
#
#cutoff : int
#	The distance to include the residue inside
#
#Return
#----------
#bool_array 
#   Numpy array of boolean
def cutoff(u,target,cutoff):
	start_time = time.time()
	dist_array = np.empty((373,len(u.trajectory)))
	bool_array = np.full(373,False)
	dist_array = np.insert(dist_array,0,np.arange(5,378,1.0),axis=1)
	for ts in u.trajectory:
		now = time.time()
		sys.stdout.write('\r')
		sys.stdout.write(f"{ts.frame/len(u.trajectory)*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
		sys.stdout.flush()
		last_time = time.time()
		cog_target = target.atoms.center_of_geometry()
		for i in range(373) :
			ag1 = u.residues[i-1]
			dist =  np.linalg.norm(ag1.atoms.center_of_geometry() - cog_target )
			if dist < cutoff + 10 :
				dist = minDist(ag1,target,dist) 
			dist_array[i][ts.frame+1] = dist
			if (dist < cutoff):
				bool_array[i] = True
	dist_array = dist_array[bool_array]
	return dist_array

	
#Calculate dihedral
#
#Parameters
#-----------
#p0 : np array
#    coordinates of point 0
#
#p1 : np array
#    coordinates of point 0
#
#p2 : np array
#    coordinates of point 0
#
#p3 : np array
#    coordinates of point 0
#
#Return
#----------
#float
#   angle of the dihedral
def calculateDihedral(p0,p1,p2,p3):
	b0 = -1.0*(p1 - p0)
	b1 = p2 - p1
	b2 = p3 - p2
	b1 /= np.linalg.norm(b1)
	v = b0 - np.dot(b0, b1)*b1
	w = b2 - np.dot(b2, b1)*b1
	x = np.dot(v, w)
	y = np.dot(np.cross(b1,v),w)
	return np.degrees(np.arctan2(y, x))


if __name__=="__main__":
	pdbfile = args.pdbfile
	xtcfile = args.xtcfile
	outfilename = args.outputfile
	
	print("Creating universe")
	u = mda.Universe(pdbfile,xtcfile)
	
	print("Creating subdomain and several parts")
	sd1 = u.residues[:31] + u.residues[65:133] + u.residues[334:372]
	sd2 = u.residues[31:65]
	sd3 = u.residues[133:178] + u.residues[259:334]
	sd4 = u.residues[178:259]
	s_loop = u.residues[6:12]
	d_loop = u.residues[33:48]
	h_loop = u.residues[65:74]
	p_loop = u.residues[104:110] #Check that
	g_loop = u.residues[149:157]
	w_loop = u.residues[160:168]
	c_terminus = u.residues[344:371]

	print("Calculatin dihedral over time")
	value = []
	start_time = time.time()
	for ts in u.trajectory:
		now = time.time()
		sys.stdout.write('\r')
		sys.stdout.write(f"{ts.frame/len(u.trajectory)*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
		sys.stdout.flush()
		value.append(calculateDihedral(sd4.atoms.center_of_geometry(),sd3.atoms.center_of_geometry(),sd1.atoms.center_of_geometry(),sd2.atoms.center_of_geometry()))
	
	print('Saving image')
	plt.plot(value)
	plt.xlabel('time')
	plt.ylabel('dihedral')
	plt.title(outfilename)
	plt.savefig(outfilename+'.png')
	plt.close()
	
	print('Saving data in .csv')
	pd.DataFrame(value).to_csv(outfilename+".csv")

	print("Program finished")
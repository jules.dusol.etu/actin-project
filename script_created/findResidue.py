#python findResidue.py -pdb frame0.pdb -xtc clean.xtc -f True -l True -c 10 -t mg -o test
import sys
import argparse
import os
import matplotlib.pyplot as plt
import itertools
import numpy as np
import pandas as pd
import MDAnalysis as mda
from MDAnalysis.analysis import align, rms
import time

#Prendre en compte water ?


parser=argparse.ArgumentParser(prog="dihedral.py")
parser.add_argument("-pdb","--pdbfile", help="topology file that contains the data;" )
parser.add_argument("-xtc","--xtcfile", help="trajectory file that contains the data;" )
parser.add_argument("-f","--filament", help="Is this a filament or not ?")
parser.add_argument("-l","--ligand", help="Is this with ligand or not ?")
parser.add_argument("-a","--atom", help="Is the target an atom or not ?")
parser.add_argument("-w","--water", help="Is the water included or not ?")
parser.add_argument("-c","--cutoff", help="cutoff distance to include residue in")
parser.add_argument("-t","--target", help="Target from which checking the distance")
parser.add_argument("-b","--begin", help="Frame to start from")
parser.add_argument("-e","--end", help="Frame to finish")
parser.add_argument("-o","--outputfile",help="file for output names;")
args = parser.parse_args(sys.argv[1:])

#Set up an absolute Universe
#
#Based on https://userguide.mdanalysis.org/stable/examples/analysis/alignment_and_rms/aligning_trajectory_to_frame.html
#
#Parameters
#-----------
#xtc : string
#    name of the xtc file
#
#Return
#----------
#Universe
#   Universe object from MDAnalysis
def bigBang(pdb,xtc):
	ref = mda.Universe(pdb,xtc)
	mobile = mda.Universe(pdb,xtc)
	ref.trajectory[0]
	for i in range(len(ref.trajectory)) :
		mobile.trajectory[i]
		aligner = mda.analysis.align.AlignTraj(mobile, ref,select = 'name CA', in_memory=True).run()
	return mobile

#Calculate minimum atom distance between two residues and return closest atom
#
#
#Parameters
#-----------
#ag1 : AtomGroup
#    First AtomGroup
#
#ag2 : AtomGroup
#	Second AtomGroup
#
#Return
#----------
#float
#   The smallest distance
def minDist(ag1,ag2,shortest_dist):
	for atom1 in ag1.atoms:
		for atom2 in ag2.atoms:
			new_dist = np.sqrt(np.sum((atom1.position-atom2.position)**2, axis=0))
			if new_dist < shortest_dist:
				shortest_dist = new_dist
	return shortest_dist

	
#Check how much and how many residue are in cutoff distance
#
#Parameters
#-----------
#u : Universe
#    Universe object containing all the residue to check
#
#target : atom selection
#	The target from which to check the cutoff distance
#
#cutoff : int
#	The distance to include the residue inside
#
#Return
#----------
#bool_array 
#   Numpy array of boolean
def cutoff(u,target,cutoff,b,e):
	start_time = time.time()
	dist_array = np.empty((373,e - b))
	bool_array = np.full(373,False)
	dist_array = np.insert(dist_array,0,np.arange(5,378,1.0),axis=1)
	for ts in u.trajectory[b:e]:
		now = time.time()
		sys.stdout.write('\r')
		sys.stdout.write(f"{ts.frame/len(u.trajectory[b:e])*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
		sys.stdout.flush()
		last_time = time.time()
		cog_target = target.atoms.center_of_geometry()
		for i in range(373) :
			ag1 = u.residues[i-1]
			dist =  np.linalg.norm(ag1.atoms.center_of_geometry() - cog_target )
			if dist < cutoff + 10 :
				dist = minDist(ag1,target,dist) 
			dist_array[i][ts.frame + 1 - b] = dist
			if (dist < cutoff):
				bool_array[i] = True
	print('\n')
	dist_array = dist_array[bool_array]
	return dist_array
	
#Calculate minimum atom distance between two residues and return closest atom
#
#
#Parameters
#-----------
#ag1 : AtomGroup
#    First AtomGroup
#
#ag2 : AtomGroup
#	Second AtomGroup
#
#Return
#----------
#float
#   The smallest distance
def minDist_atom(ag1,ag2,shortest_dist):
	for atom1 in ag1.atoms:
		new_dist = np.sqrt(np.sum((atom1.position-ag2.position)**2, axis=0))
		if new_dist < shortest_dist:
			shortest_dist = new_dist
	return shortest_dist
	
#Check how much and how many residue are in cutoff distance
#
#Parameters
#-----------
#u : Universe
#    Universe object containing all the residue to check
#
#target : atom selection
#	The target from which to check the cutoff distance
#
#cutoff : int
#	The distance to include the residue inside
#
#b : int
#	The beginning of simulation
#
#e : int
#	The end of simulation
#
#Return
#----------
#bool_array 
#   Numpy array of boolean
def cutoff_atom(u,target,cutoff,b,e):
	start_time = time.time()
	dist_array = np.empty((373,e - b))
	bool_array = np.full(373,False)
	dist_array = np.insert(dist_array,0,np.arange(5,378,1.0),axis=1)
	for ts in u.trajectory[b:e]:
		now = time.time()
		sys.stdout.write('\r')
		sys.stdout.write(f"{ts.frame/len(u.trajectory[b:e])*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
		sys.stdout.flush()
		for i in range(373) :
			ag1 = u.residues[i-1]
			dist =  np.linalg.norm(ag1.atoms.center_of_geometry() - target.position )
			if dist < cutoff + 10 :
				dist = minDist_atom(ag1,target,dist) 
			dist_array[i][ts.frame + 1 - b ] = dist
			if (dist < cutoff):
				bool_array[i] = True
	print('\n')
	dist_array = dist_array[bool_array]
	return dist_array


if __name__=="__main__":
	pdbfile = args.pdbfile
	xtcfile = args.xtcfile
	target = args.target
	if (args.begin):
		b = int(args.begin)
	else:
		b = 0
	outfilename = args.outputfile
	
	print("Creating universe")
	u = mda.Universe(pdbfile,xtcfile)
	
	if (args.end):
		e = int(args.end)
	else:
		e = len(u.trajectory)
	
	print("Creating subdomain and several parts")
	sd1 = u.residues[:31] + u.residues[65:133] + u.residues[334:372]
	sd2 = u.residues[31:65]
	sd3 = u.residues[133:178] + u.residues[259:334]
	sd4 = u.residues[178:259]
	s_loop = u.residues[6:12]
	d_loop = u.residues[33:48]
	h_loop = u.residues[65:74]
	p_loop = u.residues[104:110] #Check that
	g_loop = u.residues[149:157]
	w_loop = u.residues[160:168]
	c_terminus = u.residues[344:371]
	if (args.ligand) :
		if (args.filament) :
			ligand = u.residues[373*5]
			mg = u.residues[372*5]
		else :
			ligand = u.residues[373]
			mg = u.residues[372]
	if (target == 'mg'):
		target = mg
	elif (target == 'ligand'):
		target = ligand
	elif (target == "pa"):
		target = ligand.atoms[0]
	elif (target == "pb"):
		target = ligand.atoms[1]
	elif (target == "pg"):
		target = ligand.atoms[2]
	elif (target == "c5"):
		if ligand.resname == "ATP" :
			target = ligand.atoms[3]
		if ligand.resname == "ADP" :
			target = ligand.atoms[2]
		if ligand.resname == "AMP" :
			target = ligand.atoms[4]
	elif (args.atom):
		target = u.residues.atoms[int(target)]
	else:
		target = u.residues[int(target)-5]

	print("Searching for residues within cutoff")
	cutoff_dist = int(args.cutoff)
	if (type(target) == type(u.residues[1])):
		data = cutoff(u,target,cutoff_dist,b,e)
	elif (type(target) == type(u.residues.atoms[1])):
		data = cutoff_atom(u,target,cutoff_dist,b,e)
	bool = data[:,1:] < cutoff_dist
	resid_within = data[:,0]
	names = []
	for resid in resid_within:
		names.append(u.residues[int(resid)-6].resname+str(int(resid-1)))

	print("Plotting boolean array")
	plt.rcParams["figure.autolayout"] = True
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.imshow(bool, aspect='auto', cmap="gray", interpolation='nearest')
	ax.set_yticks(np.arange(len(names)))
	ax.set_yticklabels(names)
	plt.savefig(outfilename+'_all.png')
	plt.close()

	print("Plotting each distance to residue over time")
	i = 0
	while i<len(names):
		fig = plt.figure()
		plt.xlabel("time")
		plt.ylabel("distance")
		resid = resid_within[i]
		plt.title(names[i])
		plt.plot(data[i,1:])
		plt.savefig(outfilename+'_'+names[i]+'.png')
		plt.close()
		i+=1
	
	print('Saving data in .csv')
	df = pd.DataFrame(data[:,1:])
	df.index = names
	df.to_csv(outfilename+".csv")

	if(args.water):
		print("Analyzing water")
		start_time = time.time()
		list_water = {}
		beginning = 373
		if args.ligand:
			beginning += 1
		if args.filament:
			beginning = beginning *5
		for ts in u.trajectory[b:e]:
			now = time.time()
			sys.stdout.write('\r')
			sys.stdout.write(f"{ts.frame/len(u.trajectory[b:e])*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
			sys.stdout.flush()
			last_time = time.time()
			if (args.atom):
				cog_target = target.position
			else:
				cog_target = target.atoms.center_of_geometry()
			for res in u.residues[beginning:]:
				dist =  np.linalg.norm(res.atoms.center_of_geometry() - cog_target )
				if dist < cutoff_dist :
					name = res.resname+str(res.resid)
					if name not in list_water:
						list_water[name] = [],[]
					list_water[name][0].append(dist)
					list_water[name][1].append(ts.frame - b)
		
		print("\nPlotting each distance to water over time")
		for water in list_water:
			fig = plt.figure()
			plt.xlabel("time")
			plt.ylabel("distance")
			plt.title(water)
			plt.plot(list_water[water][1],list_water[water][0])
			plt.savefig(outfilename+'_'+water+'.png')
			plt.close()
		
		print('Saving data in .csv')
		df = pd.DataFrame(list_water)
		df.to_csv(outfilename+"_water.csv")
	print("Program finished")
#python test.py -pdb frame0.pdb -xtc clean.xtc -f True -l True -c 10 -t mg -o test
import sys
import argparse
import os
import matplotlib.pyplot as plt
import itertools
import numpy as np
import pandas as pd
import time

#Prendre en compte water ?


parser=argparse.ArgumentParser(prog="dihedral.py")
parser.add_argument("-f","--filename", help="file name containing data for ligand pa pb pg and c5;" )
parser.add_argument("-l","--ligand", help="type of ligand;" )
parser.add_argument("-o","--outputfile", help="name for the output file;" )
args = parser.parse_args(sys.argv[1:])


if __name__=="__main__":
	fileName = args.filename
	outputfile = args.outputfile
	ligand_type = args.ligand
	
	print("Gathering ligand data")
	ligand = pd.read_csv(fileName+"_ligand.csv",index_col = 0).T

	if ligand_type == 'ATP' :
		pa = pd.read_csv(fileName+"_pa.csv",index_col = 0).T
		pb = pd.read_csv(fileName+"_pb.csv",index_col = 0).T
		pg = pd.read_csv(fileName+"_pg.csv",index_col = 0).T
		c5 = pd.read_csv(fileName+"_c5.csv",index_col = 0).T
		
	if ligand_type == 'ADP' :
		pa = pd.read_csv(fileName+"_pa.csv",index_col = 0).T
		pb = pd.read_csv(fileName+"_pb.csv",index_col = 0).T
		c5 = pd.read_csv(fileName+"_c5.csv",index_col = 0).T
		
	if ligand_type == 'AMP' :
		pa = pd.read_csv(fileName+"_pa.csv",index_col = 0).T
		c5 = pd.read_csv(fileName+"_c5.csv",index_col = 0).T

	print('Searching for closest and plotting each')
	ts = 0
	start_time = time.time()
	for column in ligand.columns:
		value = []
		i = 0
		plt.rcParams["figure.autolayout"] = True
		fig = plt.figure()
		ax = fig.add_subplot(111)
		plt.title(column)
		plt.xlabel("time")
		plt.ylabel("distance")
		while i < len(ligand[column]):
			now = time.time()
			sys.stdout.write('\r')
			sys.stdout.write(f"{(i+ts*len(ligand[column]))/(len(ligand.columns)*len(ligand[column]))*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
			sys.stdout.flush()
			last_time = time.time()
			dist = ligand[column][i] + 10
			which = 'w'
			if column in pa.keys():
				if pa[column][i] < dist :
					dist = pa[column][i]
					which = 'b'
			if ligand_type == 'ADP' or ligand_type == 'ATP':
				if column in pb.keys():
					if pb[column][i] < dist :
						dist = pb[column][i]
						which = 'g'
			if ligand_type == 'ATP':
				if column in pg.keys():
					if pg[column][i] < dist :
						dist = pg[column][i]
						which = 'r'
			if column in c5.keys():
				if c5[column][i] < dist :
					dist = c5[column][i]
			value.append(dist)
			plt.axvline(x = i, color = which)
			i+= 1
		plt.plot(value,zorder = 2, color = 'black')
		if ligand_type == 'ATP':
			plt.legend(['Adenosine', 'Pa', 'Pb','Pg'], loc = 'upper left')
			ax = plt.gca()
			leg = ax.get_legend()
			leg.legendHandles[0].set_color('white')
			leg.legendHandles[1].set_color('blue')	
			leg.legendHandles[2].set_color('green')	
			leg.legendHandles[3].set_color('red')
		if ligand_type == 'ADP':
			plt.legend(['Adenosine', 'Pa', 'Pb'], loc = 'upper left')
			ax = plt.gca()
			leg = ax.get_legend()
			leg.legendHandles[0].set_color('white')
			leg.legendHandles[1].set_color('blue')	
			leg.legendHandles[2].set_color('green')
		if ligand_type == 'AMP':
			plt.legend(['Adenosine', 'Pa'], loc = 'upper left')
			ax = plt.gca()
			leg = ax.get_legend()
			leg.legendHandles[0].set_color('white')
			leg.legendHandles[1].set_color('blue')
		plt.savefig(outputfile+'_'+column+'.png')
		plt.close()
		ts += 1

	print("Program finished")
#python atomRelativity.py -pdb frame0.pdb -xtc clean.xtc -o test -a1 32308 -a2 5795 -a3 5797 -a4 5798
import sys
import argparse
import os
import matplotlib.pyplot as plt
import itertools
import numpy as np
import pandas as pd
import MDAnalysis as mda
from MDAnalysis.analysis import align, rms
import time



parser=argparse.ArgumentParser(prog="dihedral.py")
parser.add_argument("-pdb","--pdbfile", help="topology file that contains the data;" )
parser.add_argument("-xtc","--xtcfile", help="trajectory file that contains the data;" )
parser.add_argument("-a1","--atom1", help="Target atom number one")
parser.add_argument("-a2","--atom2", help="Target atom number two")
parser.add_argument("-a3","--atom3", help="Target atom number three")
parser.add_argument("-a4","--atom4", help="Target atom number four")
parser.add_argument("-o","--outputfile",help="file for output names;")
parser.add_argument("-b","--begin", help="Frame to start from")
parser.add_argument("-e","--end", help="Frame to finish")
args = parser.parse_args(sys.argv[1:])

#Calculate dihedral
#
#Parameters
#-----------
#p0 : np array
#    coordinates of point 0
#
#p1 : np array
#    coordinates of point 1
#
#p2 : np array
#    coordinates of point 2
#
#p3 : np array
#    coordinates of point 3
#
#Return
#----------
#float
#   angle of the dihedral
def calculateDihedral(p0,p1,p2,p3):
	b0 = -1.0*(p1 - p0)
	b1 = p2 - p1
	b2 = p3 - p2
	b1 /= np.linalg.norm(b1)
	v = b0 - np.dot(b0, b1)*b1
	w = b2 - np.dot(b2, b1)*b1
	x = np.dot(v, w)
	y = np.dot(np.cross(b1,v),w)
	return np.degrees(np.arctan2(y, x))
	
#Calculate angle
#
#Parameters
#-----------
#p0 : np array
#    coordinates of point 0
#
#p1 : np array
#    coordinates of point 1
#
#p2 : np array
#    coordinates of point 2
#Return
#----------
#float
#   angle of the dihedral
def calculateAngle(p0,p1,p2):
	ba = p0 - p1
	bc = p2 - p1
	cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
	return np.degrees(np.arccos(cosine_angle))
	
#Calculate distance
#
#Parameters
#-----------
#p0 : np array
#    coordinates of point 0
#
#p1 : np array
#    coordinates of point 1
#
#Return
#----------
#float
#   angle of the dihedral
def calculateDistance(p0,p1):
	squared_dist = np.sum((p0-p1)**2, axis=0)
	return np.sqrt(squared_dist)


if __name__=="__main__":
	pdbfile = args.pdbfile
	xtcfile = args.xtcfile
	outfilename = args.outputfile
	if (args.begin):
		b = int(args.begin)
	else:
		b = 0
	
	print("Creating universe")
	u = mda.Universe(pdbfile,xtcfile)
	if (args.end):
		e = int(args.end)
	else:
		e = len(u.trajectory)

	print("Creating subdomain and several parts")
	atom1 = u.residues.atoms[int(args.atom1)-1]
	atom2 = u.residues.atoms[int(args.atom2)-1]
	atom3 = u.residues.atoms[int(args.atom3)-1]
	atom4 = u.residues.atoms[int(args.atom4)-1]

	print("Calculatin dihedral, angle and distance over time")
	start_time = time.time()
	data = np.empty((e - b,4))
	for ts in u.trajectory[b:e]:
		now = time.time()
		sys.stdout.write('\r')
		sys.stdout.write(f"{ts.frame/(e - b)*100:.2f}% completed, ({int(now - start_time)} seconds of execution)")
		sys.stdout.flush()
		last_time = time.time()
		data[ts.frame][0] = calculateDihedral(atom1.position,atom2.position,atom3.position,atom4.position)
		data[ts.frame][1] = calculateAngle(atom1.position,atom2.position,atom3.position)
		data[ts.frame][2] = calculateDistance(atom1.position,atom2.position)
		data[ts.frame][3] = ts.frame
	print('\n')
		
	print("Plotting information")
	plt.ylim([0,360])
	plt.ylabel("dihedral")
	plt.xlim([-180,180])
	plt.xlabel("angle")
	plt.title("dihedral an angle ploting over time")
	plt.scatter(data[:,0],data[:,1],c=data[:,3],cmap='inferno',vmin=0 ,vmax = e - b,s=2)
	plt.colorbar()
	plt.savefig(outfilename+'_dih&ang.png')
	plt.close()
	
	plt.ylim([0,10])
	plt.xlabel("time")
	plt.ylabel("distance")
	plt.title("distance over time")
	plt.plot(data[:,2])
	plt.savefig(outfilename+'_distance.png')
	plt.close()
	
	print('Saving data in .csv')
	df = pd.DataFrame(data[:,:3])
	df.columns = ['dihedral','angle','distance']
	df.to_csv(outfilename+".csv")

	print("Program finished")
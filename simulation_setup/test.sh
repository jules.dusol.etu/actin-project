#!/bin/bash -l
#SBATCH --job-name=filATP
#SBATCH -t 00-00:12:00
#SBATCH -p gputest
#SBATCH --ntasks=1
#SBATCH --mem=2000
#SBATCH --cpus-per-task=40
#SBATCH --ntasks-per-node=1
#SBATCH --nodes=1
#SBATCH --gres=gpu:v100:4
#SBATCH -o output.%j.txt
#SBATCH -e errors.%j.txt
#SBATCH --account=panbazha


set -e

module load gcc/11.3.0  openmpi/4.1.4 gromacs/2022.4

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun $@
